﻿using System.Collections.Generic;
using UnityEngine;

namespace GCommon
{
    public class UIBaseController : MonoBehaviour
    {
        // 父节点
        public UIBaseController ParentController { get; private set; }
        public bool IsInQueue { get; internal set; }
        public bool IsOpened { get; internal set; }

        // 控制层级
        public ENavigationUILevel CurNavigationLevel = ENavigationUILevel.MaxLevel;

        protected List<UIBaseController> m_ChildControllers = null;

        protected BitArray m_UIGroup;

        private uint VISIBILITY_STATE_GROUP     = (1 << 0);
        private uint VISIBILITY_STATE_SHOWHIDE  = (1 << 1);
        private BitArrayBoolean m_StoredActiveState;

        protected virtual void Awake()
        {
            m_UIGroup = new BitArray();
            m_StoredActiveState = new BitArrayBoolean(true , BitArrayBoolean.EBitArrayBooleanMode.AND_TURE);
            OnUIInit();
            IsOpened = false;
        }
        public virtual void Close()
        {
            if (UIBaseScene.Instance == null)
            {
                //no ui scene
                if (ParentController != null)
                {
                    ParentController.RemoveChild(this);
                }
                UIClose();
                UIDestory();
            }
            else
            {
                UIBaseScene.Instance.CloseUI(this);
            }
        }
        internal void UIOpen()
        {
            IsOpened = true;
            OnUIOpen();
        }
        internal void UIClose()
        {
            if (m_ChildControllers != null)
            {
                foreach (UIBaseController childCon in m_ChildControllers)
                {
                    if (childCon != null)
                    {
                        childCon.UIClose();
                    }
                }
            }
            OnUIClose();
            this.gameObject.transform.parent = null;
        }
        internal void UIDestory()
        {
            if (m_ChildControllers != null)
            {
                foreach (UIBaseController childCon in m_ChildControllers)
                {
                    childCon.ParentController = null;
                    childCon.UIDestory();
                }
                m_ChildControllers = null;
            }
            OnUIDestory();
            Destroy(this.gameObject);
        }
        internal void UIActiveGroupSwitch(uint activeGroup)
        {
            if(m_UIGroup.GetValue() == BitArray.NONE)
            {
                m_StoredActiveState.SetBooleanValue(VISIBILITY_STATE_GROUP, true);
            }
            else
            {
                if (m_UIGroup.HasFlag(activeGroup))
                {
                    m_StoredActiveState.SetBooleanValue(VISIBILITY_STATE_GROUP, true);
                }
                else
                {
                    m_StoredActiveState.SetBooleanValue(VISIBILITY_STATE_GROUP, false);
                }
            }
            RefreshVisibility();
            if (m_ChildControllers != null)
            {
                foreach (UIBaseController childCon in m_ChildControllers)
                {
                    childCon.UIActiveGroupSwitch(activeGroup);
                }
            }
            OnUIActiveGroupSwitch(activeGroup);
        }
        // Retrieve the instantiated view prefab object 
        public GameObject GetViewRootObject()
        {
            return this.gameObject;
        }

        // Retrieve the instantiated view prefab object transform
        public Transform GetViewRootTransform()
        {
            return this.gameObject.transform;
        }

        protected virtual T OpenChildController<T>(Transform parent, Vector3 localPosition = default(Vector3)) where T: UIBaseController
        {
            if(m_ChildControllers == null)
            {
                m_ChildControllers = new List<UIBaseController>();
            }
            T con = UIBaseScene.Instance.OpenUI<T>(parent, localPosition, true);
            if (con != null)
            {
                con.ParentController = this;
                m_ChildControllers.Add(con);
            }
            return con;
        }
        internal void RemoveChild(UIBaseController controller)
        {
            if (controller.ParentController == this && m_ChildControllers != null)
            {
                m_ChildControllers.Remove(controller);
            }
        }

        protected virtual void OnUIInit()
        {
        }
        protected virtual void OnUIOpen()
        {
        }
        protected virtual void OnUIClose()
        {

        }
        protected virtual void OnUIDestory()
        {
        }
        protected virtual void OnUIActiveGroupSwitch(uint activeGroup)
        {

        }
        protected virtual void OnVisibilityChanged()
        {

        }
        public void Show()
        {
            m_StoredActiveState.SetBooleanValue(VISIBILITY_STATE_SHOWHIDE, true);
            RefreshVisibility();
        }

        public void Hide()
        {
            m_StoredActiveState.SetBooleanValue(VISIBILITY_STATE_SHOWHIDE, false);
            RefreshVisibility();
        }
        public void SetVisibility(bool v)
        {
            m_StoredActiveState.SetBooleanValue(VISIBILITY_STATE_SHOWHIDE, v);
            RefreshVisibility();
        }
        public void SwitchGroup(uint activeGroup)
        {
            UIActiveGroupSwitch(activeGroup);
        }
        public bool IsVisible()
        {
            return this.gameObject.activeSelf;
        }
        public UIBaseController SetUIGroup(uint groupFlags)
        {
            m_UIGroup.AddFlag(groupFlags);
            return this;
        }
        private void RefreshVisibility()
        {
            bool lstVisibilityState = IsVisible();
            bool curVisibilityState = m_StoredActiveState.GetBooleanValue();
            GetViewRootObject().SetActive(curVisibilityState);
            if (curVisibilityState != lstVisibilityState)
            {
                OnVisibilityChanged();
            }
        }
    }
}
