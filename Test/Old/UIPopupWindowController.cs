﻿using System.Collections.Generic;
using UnityEngine;

namespace GCommon
{
    public class UIPopupWindowController : UIBaseController
    {
        //protected List<UITweener> m_PopupTweener;
        //protected UIPanel m_MainPanel;
        //protected UIPanel[] m_ChildPanels;

        protected virtual int MinGroupDepth()
        {
            return 30;
        }

        protected virtual bool isMessageBox()
        {
            return false;
        }

        protected virtual string AnimationConfogPath()
        {
            return "UI/PopupAnimConfig";
        }

        protected virtual bool UseCustomizedAnimation()
        {
            return false;
        }

        protected override void Awake()
        {
            base.Awake();

            //m_MainPanel = gameObject.GetComponent<UIPanel>();
            //if (m_MainPanel == null)
            //{
            //    m_MainPanel = gameObject.AddComponent<UIPanel>();
            //}
            //m_ChildPanels = m_MainPanel.GetComponentsInChildren<UIPanel>(true);

            //if (!UseCustomizedAnimation())
            //{
            //    AddTweenByConfig(m_MainPanel);
            //}
        }

        //public void AddTweenByConfig(UIPanel mainPanel)
        //{
        //    string path = AnimationConfogPath();
        //    GameObject configGameObject = ResourceManager.instance.GetResource(path, true) as GameObject;

        //    if (configGameObject == null)
        //    {
        //        return;
        //    }

        //    var components = configGameObject.GetComponents<UITweener>();
        //    for (int i = 0; i < components.Length; i++)
        //    {
        //        UITweener tmpTween = null;
        //        if (components[i] is TweenAlpha)
        //        {
        //            tmpTween = m_MainPanel.gameObject.AddComponent<TweenAlpha>();
        //            (tmpTween as TweenAlpha).from = (components[i] as TweenAlpha).from;
        //            (tmpTween as TweenAlpha).to = (components[i] as TweenAlpha).to;
        //        }
        //        else if (components[i] is TweenScale)
        //        {
        //            tmpTween = m_MainPanel.gameObject.AddComponent<TweenScale>();
        //            (tmpTween as TweenScale).from = (components[i] as TweenScale).from;
        //            (tmpTween as TweenScale).to = (components[i] as TweenScale).to;
        //        }
        //        tmpTween.style = components[i].style;
        //        tmpTween.animationCurve = components[i].animationCurve;
        //        tmpTween.duration = components[i].duration;
        //        tmpTween.delay = components[i].delay;
        //        tmpTween.tweenGroup = components[i].tweenGroup;
        //        tmpTween.ignoreTimeScale = components[i].ignoreTimeScale;
        //    }
        //}
        
        public int GetMaxPanelDepth()
        {
            //UIPanel panel = gameObject.GetComponent<UIPanel>();
            //if (panel == null)
            //{
                return 0;
            //}

            //int maxDepth = panel.depth;

            //if (m_MainPanel == null)
            //    m_MainPanel = panel;

            //if (m_ChildPanels == null)
            //{
            //    m_ChildPanels = m_MainPanel.GetComponentsInChildren<UIPanel>(true);
            //}

            //if (m_ChildPanels == null
            //    || m_ChildPanels.Length <= 0)
            //    return maxDepth;

            //for (int i = 0; i < m_ChildPanels.Length; i++)
            //{
            //    if (m_ChildPanels[i].depth > maxDepth)
            //    {
            //        maxDepth = m_ChildPanels[i].depth;
            //    }
            //}

            //return maxDepth;
        }

        protected override void OnUIOpen()
        {
            base.OnUIOpen();

            if (UIBaseScene.Instance != null)
            {
                //UIPopupWindowController prevPopupController = isMessageBox() ? UIBaseScene.Instance.CurrentMessageBoxController : UIBaseScene.Instance.CurrentPopupController;

                //int startDepth = MinGroupDepth();

                //if (prevPopupController != null)
                //{
                //    startDepth = prevPopupController.GetMaxPanelDepth();
                //}

                //m_MainPanel.depth = startDepth + 1;

                //for (int i = 0; i < m_ChildPanels.Length; i++)
                //{
                //    if (m_ChildPanels[i] == m_MainPanel)
                //    {
                //        continue;
                //    }
                //    m_ChildPanels[i].depth = m_MainPanel.depth + m_ChildPanels[i].depth;
                //}
            }
        }

        protected override void OnUIClose()
        {
            base.OnUIClose();
            if (UIBaseScene.Instance != null)
            {
                UIBaseScene.Instance.SetPreviousPopupWindows(isMessageBox());
            }
        }

        protected override T OpenChildController<T>(Transform parent, Vector3 localPosition = default(Vector3))
        {
            T controller = base.OpenChildController<T>(parent, localPosition);

            //UIPanel[] tmpChildPanels = controller.GetComponentsInChildren<UIPanel>(true);

            //int startDepth = GetMaxPanelDepth();

            //for (int i = 0; i < tmpChildPanels.Length; i++)
            //{
            //    tmpChildPanels[i].depth = startDepth + tmpChildPanels[i].depth + 1;
            //}

            ////refresh child panels
            //m_ChildPanels = m_MainPanel.GetComponentsInChildren<UIPanel>(true);

            return controller;
        }

        public virtual void ClosedByEsc()
        {
            
        }

        public virtual bool CanSavedByNavigation()
        {
            return true;
        }
    }
}
