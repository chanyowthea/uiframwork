﻿using GCommon;
using System;

namespace COW
{
    public class UIPopupMessageBoxController : UIPopupSpecialWindowController
    {
        public enum EButtonStyle
        {
            None, OKOnly, OKCancel, OKClose,
        }
        public enum EStandardMessageBoxType
        {
            Info, Suggestion, Warning, Error
        }
        public class MessageBoxInfo
        {
            public string Title;
            public string Info;
            public string OKText;
            public string CancelText;
            public EButtonStyle ButtonStyle;
            public Action OnOK;
            public Action OnCancel;
            public bool NoCloseButton = false;
        }
        //private UIPopupMessageBoxView m_View;
        private EButtonStyle m_ButtonStyle;
        private bool m_NoCloseButton = false;
        private Action m_OnOK = null;
        private Action m_OnCancel = null;
        private bool m_HideOnly = false;
        //public static ResourceID GetResourceID()
        //{
        //    return ResourceIDDef.UIPOPUPMESSAGEBOX;
        //}

        protected override void OnUIInit()
        {
            base.OnUIInit();
            //m_View = CreateView<UIPopupMessageBoxView>();
            //EventDelegate.Add(m_View.BtnOK.onClick, OnBtnOKClick);
            //EventDelegate.Add(m_View.BtnCancel.onClick, OnBtnCancelClick);
            //EventDelegate.Add(m_View.BtnClose.onClick, OnBtnCancelClick);
        }
        protected override void OnUIClose()
        {
            base.OnUIClose();
            m_OnOK = null;
            m_OnCancel = null;
        }

        public void SetCloseMethod(bool isHideOnly)
        {
            m_HideOnly = isHideOnly;
        }
        public void ShowStandardMessageBox(string info, EStandardMessageBoxType mbType, Action onOK = null, Action onCancel = null, EButtonStyle buttonStyle = EButtonStyle.OKOnly)
        {
            //if(mbType == EStandardMessageBoxType.Error)
            //{
            //    LocManager.instance.DoLoc(m_View.Title, "TXT_MESSAGEBOX_ERROR");
            //}
            //else if(mbType == EStandardMessageBoxType.Warning)
            //{
            //    LocManager.instance.DoLoc(m_View.Title, "TXT_MESSAGEBOX_WARNING");
            //}
            //else if(mbType == EStandardMessageBoxType.Suggestion)
            //{
            //    LocManager.instance.DoLoc(m_View.Title, "TXT_MESSAGEBOX_SUGGESTION");
            //}
            //else
            //{
            //    LocManager.instance.DoLoc(m_View.Title, "TXT_MESSAGEBOX_INFO");
            //}
            //m_View.Info.Clear();
            //m_View.Info.Add(info);
            //m_ButtonStyle = buttonStyle;
            
            //LocManager.instance.DoLoc(m_View.BtnOKLabel, "TXT_MESSAGEBOX_OK");
            //LocManager.instance.DoLoc(m_View.BtnCancelLabel, "TXT_MESSAGEBOX_CANCEL");
            //m_OnOK = onOK;
            //m_OnCancel = onCancel;
            UpdateView();
            Show();
        }
        public void ShowMessageBox(string title, string info, EButtonStyle buttonStyle = EButtonStyle.OKCancel, Action onOK = null, Action onCancel = null)
        {
            //m_View.Title.text = title;
            //m_View.Info.Clear();
            //m_View.Info.Add(info);
            //m_ButtonStyle = buttonStyle;
            //LocManager.instance.DoLoc(m_View.BtnOKLabel, "TXT_MESSAGEBOX_OK");
            //LocManager.instance.DoLoc(m_View.BtnCancelLabel, "TXT_MESSAGEBOX_CANCEL");
            //m_OnOK = onOK;
            //m_OnCancel = onCancel;
            UpdateView();
            Show();
        }
        public void ShowMessageBox(MessageBoxInfo info)
        {
            //m_View.Title.text = info.Title;
            //m_View.Info.Clear();
            //m_View.Info.Add(info.Info);
            //m_View.BtnOKLabel.text = info.OKText;
            //m_View.BtnCancelLabel.text = info.CancelText;
            //m_ButtonStyle = info.ButtonStyle;
            //m_NoCloseButton = info.NoCloseButton;
            //m_OnOK = info.OnOK;
            //m_OnCancel = info.OnCancel;
            UpdateView();
            Show();
        }
        public override void Close()
        {
            if(m_HideOnly)
            {
                return;
            }
            base.Close();
        }
        private void OnBtnOKClick()
        {
            Hide();
            if (m_OnOK != null)
            {
                Action okCB = m_OnOK;
                m_OnOK = null;
                okCB.Invoke();
            }
            Close();
        }
        private void OnBtnCancelClick()
        {
            Hide();
            if (m_OnCancel != null)
            {
                Action cancelCB = m_OnCancel;
                m_OnCancel = null;
                cancelCB.Invoke();
            }
            Close();
        }
        private void UpdateView()
        {
            //m_View.Info.scrollBar.gameObject.SetActive(m_View.Info.NeedScrollBar());
            if (m_ButtonStyle == EButtonStyle.OKCancel)
            {
                //m_View.BtnCloseGO.SetActive(m_NoCloseButton == false);
                //m_View.BtnOKGO.SetActive(true);
                //m_View.BtnOKSprite.leftAnchor.absolute = -13;
                //m_View.BtnOKSprite.rightAnchor.absolute = 213;
                //m_View.BtnCancelGO.SetActive(true);
                //m_View.BtnCancelSprite.leftAnchor.absolute = -210;
                //m_View.BtnCancelSprite.rightAnchor.absolute = 16;
                //m_View.TextArea.bottomAnchor.absolute = 95;
            }
            else if (m_ButtonStyle == EButtonStyle.OKOnly)
            {
                //m_View.BtnCancelGO.SetActive(false);
                //m_View.BtnCloseGO.SetActive(false);
                //m_View.BtnOKGO.SetActive(true);
                //m_View.BtnOKSprite.leftAnchor.absolute = -113;
                //m_View.BtnOKSprite.rightAnchor.absolute = 114;
                //m_View.TextArea.bottomAnchor.absolute = 95;
            }
            else if(m_ButtonStyle == EButtonStyle.OKClose)
            {
                //m_View.BtnCancelGO.SetActive(false);
                //m_View.BtnCloseGO.SetActive(true);
                //m_View.BtnOKGO.SetActive(true);
                //m_View.BtnOKSprite.leftAnchor.absolute = -113;
                //m_View.BtnOKSprite.rightAnchor.absolute = 114;
            }
            else
            {
                //m_View.BtnCancelGO.SetActive(false);
                //m_View.BtnCloseGO.SetActive(false);
                //m_View.BtnOKGO.SetActive(false);
                //m_View.TextArea.bottomAnchor.absolute = 30;
            }
        }

        public override void OnBackButtonClick()
        {
            base.OnBackButtonClick();

            if (m_ButtonStyle == EButtonStyle.OKCancel)
            {
                OnBtnCancelClick();
            }
            else if (m_ButtonStyle == EButtonStyle.OKClose)
            {
                Close();
            }
            else if (m_ButtonStyle == EButtonStyle.OKOnly)
            {
                OnBtnOKClick();
            }
            else if (m_ButtonStyle == EButtonStyle.None)
            {
                //do nothing
            }
        }
    }
}
