﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GCommon
{
    public interface IUIModelDataChangeObserver
    {
        void OnDataChanged(UIBaseModel model, uint propID, params object[] param);
        uint GetInterestedPropID(UIBaseModel model);
    }
    public abstract class UIBaseModel
    {
        private List<IUIModelDataChangeObserver> m_Observers;
        public void RegisterDataChangedNotification(IUIModelDataChangeObserver observer)
        {
            if (m_Observers == null)
            {
                m_Observers = new List<IUIModelDataChangeObserver>();
            }
            if (!m_Observers.Contains(observer))
            {
                m_Observers.Add(observer);
            }
        }
        public void UnRegisterDataChangedNotification(IUIModelDataChangeObserver observer)
        {
            if(m_Observers == null)
            {
                return;
            }
            m_Observers.Remove(observer);
        }
        public virtual void NotifyDataChanged(uint propID, params object[] param)
        {
            if (m_Observers == null)
            {
                return;
            }
            for (int i = 0; i < m_Observers.Count; i++)
            {
                IUIModelDataChangeObserver observer = m_Observers[i];
                if (observer != null && BitArray.HasFlag(propID, observer.GetInterestedPropID(this)))
                {
                    observer.OnDataChanged(this, propID, param);
                }
            }
        }
        public abstract uint GetModelType();

        public virtual void Login(params object[] data)
        {
        }
        public virtual void Logout(params object[] data)
        {
        }
        public void Cleanup()
        {
            m_Observers = null; //clear all observers
            OnCleanup();
        }
        protected virtual void OnCleanup()
        {

        }
    }
}
