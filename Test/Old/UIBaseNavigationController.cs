﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GCommon
{
    public class UIBaseNavigationController : UIBaseController
    {
        protected UINavigationData m_NavigationData;
        protected bool m_IsRoot = false;
        protected override void OnUIOpen()
        {
            base.OnUIOpen();

            //Init all child controllers
        }

        public bool IsRoot
        {
            get { return m_IsRoot; }
        }

        public virtual void OnNavigationShowed(UINavigationData navigationData, bool isRoot)
        {
            m_NavigationData = navigationData;

            m_IsRoot = isRoot;

            for (int i = 0; i < m_ChildControllers.Count; i++)
            {
                m_ChildControllers[i].Show();
            }
        }

        public virtual void OnNavigationClosed()
        {
            for (int i = 0; i < m_ChildControllers.Count; i++)
            {
                m_ChildControllers[i].Hide();
            }
        }
    }
}
