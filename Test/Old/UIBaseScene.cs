﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace GCommon
{
    public enum ENavigationUILevel
    {
        RootRanel,
        FullWindowPanel,
        OverLyingPanel,
        MaxLevel,
    }


    public class UIBaseScene
    {
        public static UIBaseScene Instance = null;
        
        // 所有打开的Controller
        protected List<UIBaseController> m_Controllers;

        // 用于关闭UI的时候弹出队列中的UI,应该没啥用
        protected Queue<UIBaseController> m_QueuedController;

        // 要么新建一个类单独处理,要么本框架统一处理,不要在这里加表
        //protected List<UIPopupWindowController> m_PopupControllers;
        //protected List<UIPopupSpecialWindowController> m_MessageBoxControllers; 

        // 当前群组
        private uint m_CurActiveGroup;
        // 先前群组
        private uint m_PreviousActiveGroup;

        //private UIPopupWindowController m_CurrentPopupController;
        //public UIPopupWindowController CurrentPopupController
        //{
        //    get
        //    {
        //        return m_CurrentPopupController;
        //    }
        //}

        //private UIPopupSpecialWindowController m_CurrentMessageBoxController;

        //public UIPopupSpecialWindowController CurrentMessageBoxController
        //{
        //    get
        //    {
        //        return m_CurrentMessageBoxController;
        //    }
        //}
        //-----------------------------------------------------
        protected Dictionary<int, List<UIBaseController>> m_NavigationControllersMap = new Dictionary<int, List<UIBaseController>>() {
            { (int)ENavigationUILevel.RootRanel, new List<UIBaseController>()},
            { (int)ENavigationUILevel.FullWindowPanel, new List<UIBaseController>()},
            { (int)ENavigationUILevel.OverLyingPanel, new List<UIBaseController>()},
        };

        // navigation 好像是没有用的
        protected List<UINavigationData> m_NavigationDataStack = null;
        protected Dictionary<int, UIBaseNavigationController> m_NavControllerDic = null;
        private static int curNavIndex = 0;
        private bool mIsInitialzied = false;
        public bool Initialized
        {
            get { return mIsInitialzied; }
        }

        public delegate bool EscapeHandle();
        List<EscapeHandle> m_EscapeHandles = new List<EscapeHandle>();

        public void Init()
        {
            Instance = this;
            m_Controllers = new List<UIBaseController>();
            m_QueuedController = new Queue<UIBaseController>();
            //m_PopupControllers = new List<UIPopupWindowController>();

            //m_MessageBoxControllers = new List<UIPopupSpecialWindowController>();
            m_NavigationDataStack = new List<UINavigationData>();
            m_NavControllerDic = new Dictionary<int, UIBaseNavigationController>();

            OnInit();
            mIsInitialzied = true;
        }
        public void Destory()
        {
            if (m_Controllers != null)
            {
                foreach (UIBaseController controller in m_Controllers)
                {
                    if(controller != null)
                    {
                        controller.UIDestory();
                    }
                }
                m_Controllers.Clear();
            }
            if (m_QueuedController != null)
            {
                m_QueuedController.Clear();
            }
            OnDestory();
        }

        public virtual T PushNavigation<T>(UINavigationData navigationData, bool isRoot = false) where T: UIBaseNavigationController
        {
            //check if already in call stack
            UINavigationData m_ExistNavData = null;
            for (int i = 0; i < m_NavigationDataStack.Count; i++)
            {
                if (m_NavigationDataStack[i] != null && m_NavigationDataStack[i].ContentControllerType == typeof(T))
                {
                    m_ExistNavData = m_NavigationDataStack[i];
                    break;
                }
            }

            T navigationController = null;
            if (m_ExistNavData == null)
            {
                navigationController = OpenUIController<T>(null, isRoot ? ENavigationUILevel.RootRanel : ENavigationUILevel.FullWindowPanel, false, false, navigationData.ContentControllerType);
            }
            else
            {
                navigationController = m_NavControllerDic[m_ExistNavData.NavIndex] as T;
            }

            if (m_NavigationDataStack.Count < 1 && !isRoot)
            {
                //Debugger.LogError("Wrong Navigation Status!!!");
                return null;
            }
            if (!isRoot)
            {
                UINavigationData oldNavData = m_NavigationDataStack[m_NavigationDataStack.Count - 1];
                UIBaseNavigationController oldNavCon = null;
                if (m_NavControllerDic.TryGetValue(oldNavData.NavIndex, out oldNavCon))
                {
                    //oldNavData.SavePopupList(m_PopupControllers);
                    oldNavCon.OnNavigationClosed();
                    oldNavCon.Hide();
                }
            }

            if (m_ExistNavData != null)
            {
                navigationData.NavIndex = m_ExistNavData.NavIndex;
                m_NavigationDataStack.Remove(m_ExistNavData);
            }
            else
            {
                navigationData.NavIndex = curNavIndex;
                curNavIndex++;
            }

            if (navigationData.ContentControllerType == null)
            {
                navigationData.ContentControllerType = typeof(T);
            }

            if (navigationController != null)
            {
                navigationController.Show();
                navigationController.OnNavigationShowed(navigationData, isRoot);
                m_NavigationDataStack.Add(navigationData);
                m_NavControllerDic[navigationData.NavIndex] = navigationController;
            }

            //show transfer animation
            //if (TransferMask != null)
            //{
            //    TweenAlpha tween = TransferMask.gameObject.GetComponent<TweenAlpha>();
            //    tween.ResetToBeginning();
            //    tween.PlayForward();
            //}

            return navigationController;
        }

        public UINavigationData PopNavigation()
        {
            if (m_NavigationDataStack.Count <= 1)
            {
                return null; //cannot pop root navigation
            }
            UINavigationData popNavData = m_NavigationDataStack[m_NavigationDataStack.Count - 1]; //get last one
            UIBaseNavigationController popNavCon = m_NavControllerDic[popNavData.NavIndex];
            if (popNavData != null)
            {
                m_NavigationDataStack.Remove(popNavData);
                popNavCon.OnNavigationClosed();
                CloseUI(popNavCon);
                m_NavControllerDic.Remove(popNavData.NavIndex);
            }

            // 获取导航中的下一个UI
            UINavigationData showNavData = m_NavigationDataStack[m_NavigationDataStack.Count - 1];//get the below one
            if (showNavData != null)
            {
                UIBaseNavigationController showNavCon = m_NavControllerDic[showNavData.NavIndex];
                showNavCon.Show();
                showNavCon.OnNavigationShowed(showNavData, showNavCon.IsRoot);
            }

            //if (TransferMask != null)
            //{
            //    TweenAlpha tween = TransferMask.gameObject.GetComponent<TweenAlpha>();
            //    tween.ResetToBeginning();
            //    tween.PlayForward();
            //}
            return popNavData;
        }
        
        //if want to close a specific navigation UI
        public void CloseNavigation<T>() where T : UIBaseNavigationController
        {
            if (m_NavigationDataStack.Count <= 0)
            {
                return;
            }
            UINavigationData curNavData = m_NavigationDataStack[m_NavigationDataStack.Count - 1];

            // 仅仅通过controllertype来寻找UI
            if (curNavData.ContentControllerType == typeof(T))
            {
                PopNavigation();
                return;
            }
            UINavigationData m_ExistNavData = null;
            for (int i = 0; i < m_NavigationDataStack.Count; i++)
            {
                if (m_NavigationDataStack[i] != null && m_NavigationDataStack[i].ContentControllerType == typeof(T))
                {
                    m_ExistNavData = m_NavigationDataStack[i];
                    break;
                }
            }
            if (m_ExistNavData == null)
            {
                Debug.LogWarning(string.Format("No Such Navigation {0}", typeof(T).ToString()));
                return;
            }

            // navigation可以从中间关闭
            UIBaseNavigationController closeController = null;
            if (m_NavControllerDic.TryGetValue(m_ExistNavData.NavIndex, out closeController))
            {
                closeController.OnNavigationClosed();
                CloseUI(closeController);
                m_NavigationDataStack.Remove(m_ExistNavData);
            }
        }

        public Type CurrentNavigationType
        {
            get { return m_NavigationDataStack[m_NavigationDataStack.Count - 1].ContentControllerType; } 
        }

        public T OpenUIController<T>(Transform parent, ENavigationUILevel navigationLevel, bool isChildCon = false, bool isQueued = false, Type forceType = null) where T : UIBaseController
        {
            T uiController = OpenUI<T>(parent, isChildCon, isQueued, forceType);
            int navigationLevel_int = (int)navigationLevel;
            int maxLevel_int = (int)ENavigationUILevel.MaxLevel;


            if(navigationLevel_int<maxLevel_int)
            {
                m_NavigationControllersMap[navigationLevel_int].Add(uiController);
                uiController.CurNavigationLevel = navigationLevel;
            }
            else
            {
                //Debugger.LogError("@sn: Open Navigation UI Failed! level is too high");
            }

            return uiController;
        }

        public bool CloseUIController(ENavigationUILevel belowLevel = ENavigationUILevel.RootRanel)
        {
            int belowLevel_int = (int)belowLevel;
            int maxLevel_int = (int)ENavigationUILevel.MaxLevel;

            if (belowLevel_int >= maxLevel_int)
            {
                //Debugger.LogError("@sn: Close Navigation UI Failed! level is too high");
                return false;
            }
            
            if(m_NavigationControllersMap[belowLevel_int].Count <=0 )
            {
                //Debugger.LogWarning("@sn: Close Navigation UI Failed! Current level don't have Any Controller");
                return false;
            }

            for (int i = belowLevel_int ;i< maxLevel_int; i++)
            {
                for (int j=0;j < m_NavigationControllersMap[i].Count; j++)
                {
                    CloseUI(m_NavigationControllersMap[i][j], false);
                }
                m_NavigationControllersMap[i].Clear();
            }

            return true;
        }

        public T ShowPopupWindow<T>() where T:UIPopupWindowController
        {
            T popupController = OpenUIController<T>(null, ENavigationUILevel.OverLyingPanel);
            
            //if (popupController is COW.UIPopupMessageBoxController)
            //{
            //    m_MessageBoxControllers.Add(popupController as COW.UIPopupMessageBoxController);
            //    m_CurrentMessageBoxController = popupController as COW.UIPopupMessageBoxController;
            //}
            //else
            //{
            //    m_PopupControllers.Add(popupController);
            //    m_CurrentPopupController = popupController;
            //}
            return popupController;
        }


        public void CloseAllPopups()
        {
            CloseUIController(ENavigationUILevel.OverLyingPanel);

            //if (m_PopupControllers != null)
            //    m_PopupControllers.Clear();

            //m_CurrentPopupController = null;

            //if (m_MessageBoxControllers != null)
            //    m_MessageBoxControllers.Clear();

            //m_CurrentMessageBoxController = null;
        }

        public T OpenUI<T>(Transform parent, bool isChildCon = false, bool isQueued = false, Type forceType = null) where T : UIBaseController
        {
            UIBaseController uiCon = CreateUI<T>(forceType);
            if (uiCon == null)
            {
                return default(T);
            }
            bool delayOpen = false;
            if (UIBaseScene.Instance != null)
            {
                if (isChildCon == false)
                {
                    m_Controllers.Add(uiCon);
                    if (isQueued)
                    {
                        // 当前UI关闭的时候,会从Queue中弹出一个打开
                        if (m_QueuedController.Count > 0)
                        {
                            delayOpen = true;
                        }
                        m_QueuedController.Enqueue(uiCon);
                    }
                }
            }
            uiCon.gameObject.SetActive(true);
            uiCon.transform.parent = parent;
            uiCon.transform.localPosition = Vector3.zero;
            uiCon.transform.localScale = Vector3.one;
            uiCon.IsInQueue = isQueued;
            if (delayOpen == false)
            {
                uiCon.UIOpen();
            }
            return (T)uiCon;
        }
        public T OpenUI<T>(Transform parent, Vector3 localPosition, bool isChildCon = false, bool isQueued = false) where T : UIBaseController
        {
            UIBaseController uiCon = CreateUI<T>();
            if(uiCon == null)
            {
                return default(T);
            }
            bool delayOpen = false;
            if (UIBaseScene.Instance != null)
            {
                if (isChildCon == false)
                {
                    m_Controllers.Add(uiCon);
                    if (isQueued)
                    {
                        if (m_QueuedController.Count > 0)
                        {
                            delayOpen = true;
                        }
                        m_QueuedController.Enqueue(uiCon);
                    }
                }
            }
            Transform targetParent = null;
            //if(anchor != EUIAnchor.None)
            //{
            //    Transform anchorTrans = parent.Find(anchor.ToString());
            //    if(anchorTrans == null)
            //    {
                    //UIAnchor a = new GameObject(anchor.ToString()).AddComponent<UIAnchor>();
                    //a.transform.parent = parent;
                    //a.transform.localPosition = Vector3.zero;
                    //a.transform.localScale = Vector3.one;
                    //switch (anchor)
                    //{
                    //    case EUIAnchor.TopLeft: a.side = UIAnchor.Side.TopLeft; break;
                    //    case EUIAnchor.Top: a.side = UIAnchor.Side.Top; break;
                    //    case EUIAnchor.TopRight: a.side = UIAnchor.Side.TopRight; break;
                    //    case EUIAnchor.Left: a.side = UIAnchor.Side.Left; break;
                    //    case EUIAnchor.Center: a.side = UIAnchor.Side.Center; break;
                    //    case EUIAnchor.Right: a.side = UIAnchor.Side.Right; break;
                    //    case EUIAnchor.BottomLeft: a.side = UIAnchor.Side.BottomLeft; break;
                    //    case EUIAnchor.Bottom: a.side = UIAnchor.Side.Bottom; break;
                    //    case EUIAnchor.BottomRight: a.side = UIAnchor.Side.BottomRight; break;
                    //}
                    //a.runOnlyOnce = true;
                    //a.container = parent.gameObject;
                    //anchorTrans = a.transform;
                //}
            //    if (anchorTrans != null)
            //    {
            //        targetParent = anchorTrans;
            //    }
            //}
            if(targetParent == null)
            {
                targetParent = parent;
            }
            uiCon.transform.parent = targetParent;
            uiCon.gameObject.SetActive(true);
            uiCon.transform.localPosition = localPosition;
            uiCon.transform.localScale = Vector3.one;
            uiCon.IsInQueue = isQueued;
            if (delayOpen == false)
            {
                uiCon.UIOpen();
            }
            return (T)uiCon;
        }
        public void CloseUI(UIBaseController controller, bool shouldRemove = true)
        {
            ENavigationUILevel oldLevel = controller.CurNavigationLevel;

            if (shouldRemove && controller.CurNavigationLevel < ENavigationUILevel.MaxLevel)
            {
                if(m_NavigationControllersMap[(int)controller.CurNavigationLevel].Contains(controller))
                {
                    m_NavigationControllersMap[(int)controller.CurNavigationLevel].Remove(controller);
                }
            }



            if (UIBaseScene.Instance == null)
            {
                //no ui scene
                if (controller.ParentController != null)
                {
                    controller.ParentController.RemoveChild(controller);
                }
                controller.UIClose();
                controller.UIDestory();
            }
            else
            {
                if (controller.ParentController == null)
                {
                    m_Controllers.Remove(controller);
                }
                else
                {
                    controller.ParentController.RemoveChild(controller);
                }
                controller.UIClose();
                controller.UIDestory(); //TODO
                // 关闭这个UI,然后显示队列中下一个UI
                if (controller.IsInQueue)
                {
                    m_QueuedController.Dequeue();
                    if (m_QueuedController.Count > 0)
                    {
                        UIBaseController nextCon = m_QueuedController.Peek();
                        nextCon.UIOpen();
                    }
                }
            }
        }

        private T CreateUI<T>(Type forceType = null) where T : UIBaseController
        {
            Type type = forceType != null ? forceType : typeof(T);
            MethodInfo getResIDMethod = type.GetMethod("GetResourceID");
            //if (getResIDMethod == null)
            //{
                //Debugger.LogError("GetResourceID is not defined in controller: " + type.ToString());
                return null;
            //}
            //ResourceID resID = (ResourceID)getResIDMethod.Invoke(null, null);
            //GameObject go = null;
            //if (resID == ResourceID.INVALID)
            //{
            //    Debugger.LogWarning("empty ui " + type.ToString());
            //    go = new GameObject();
            //    go.name = type.ToString();
            //}
            //else
            //{
            //    go = InstantiateUIPrefab( resID);
            //}
            //return go.AddComponent(type) as T;
        }

        /// <summary>
        /// Instantiate specific UI prefab block via Resource ID  
        /// @TODO, to instantiate prefab via pure name string..
        /// </summary>
        /// <param name="InPrefabResID"></param>
        /// <returns></returns>
        //public UnityEngine.GameObject InstantiateUIPrefab(ResourceID InPrefabResID )
        //{
        //    //UnityEngine.Object uiRes = ResourceManager.instance.GetResource(InPrefabResID);
        //    //if (uiRes == null)
        //    //{
        //    //    Debugger.LogError("cannot find resource of: " + InPrefabResID.ToString());
        //    return null;
        //    //}
        //    //GameObject go = GameObject.Instantiate(uiRes) as GameObject;
        //    //// Remove the annoying appendix "(Cloned)" 
        //    //int iCloneStrPos = go.name.IndexOf("(Clone)");
        //    //if (iCloneStrPos >= 0)
        //    //    go.name = go.name.Remove(iCloneStrPos);

        //    //return go;
        //}

        /// <summary>
        /// Search controller matching given type 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        /*public T GetUIController<T>() where T: UIBaseController
        {
            Type type = typeof(T);

            foreach (UIBaseController controller in m_Controllers )
            {
                if (controller.GetType() == type)
                    return controller as T;
            }
            return null;
        }*/

        protected virtual Transform FindUIRoot()
        {
            return GameObject.Find("UIRoot").transform;
        }

        //private UISprite GetNavigationTransferMask()
        //{
        //    if (UIRoot == null)
        //    {
        //        UIRoot = FindUIRoot();
        //    }
        //    if (UIRoot != null)
        //    {
        //        Transform mask = UIRoot.Find("NavigationTransferMask");
        //        if (mask != null)
        //        {
        //            return mask.GetComponent<UISprite>();
        //        }
        //    }
        //    return null;
        //}

        public void SetPreviousActiveGroup()
        {
            SetActiveGroup(m_PreviousActiveGroup);
        }

        public void SetPreviousPopupWindows(bool isMessageBox)
        {
            //if (isMessageBox)
            //{
            //    if (m_MessageBoxControllers == null)
            //    {
            //        return;
            //    }

            //    if (m_MessageBoxControllers.Count > 1)
            //    {
            //        m_CurrentMessageBoxController = m_MessageBoxControllers[m_MessageBoxControllers.Count - 2];
            //        m_MessageBoxControllers.RemoveAt(m_MessageBoxControllers.Count - 1);
            //    }
            //    else
            //    {
            //        m_CurrentMessageBoxController = null;
            //        m_MessageBoxControllers.Clear();
            //    }
            //}
            //else
            //{
            //    if (m_PopupControllers == null)
            //    {
            //        return;
            //    }

            //    if (m_PopupControllers.Count > 1)
            //    {
            //        m_CurrentPopupController = m_PopupControllers[m_PopupControllers.Count - 2];
            //        m_PopupControllers.RemoveAt(m_PopupControllers.Count - 1);
            //    }
            //    else
            //    {
            //        m_CurrentPopupController = null;
            //        m_PopupControllers.Clear();
            //    }
            //}
        }

        public void SetActiveGroup(uint InActiveGroup)
        {
            if (m_CurActiveGroup == InActiveGroup)
            {
                return;
            }
            m_PreviousActiveGroup = m_CurActiveGroup;
            m_CurActiveGroup = InActiveGroup;
            foreach (UIBaseController con in m_Controllers)
            {
                con.UIActiveGroupSwitch(m_CurActiveGroup);
            }
        }
        public bool IsGroupActive(uint flag)
        {
            return BitArray.HasFlag(m_CurActiveGroup, flag);
        }
        protected virtual void OnInit()
        {
        }
        protected virtual void OnDestory()
        {
        }
        //public float GetPixelSizeAdjustment()
        //{
        //    return UIRoot.GetComponent<UIRoot>().pixelSizeAdjustment;
        //}
        public UINavigationData GetCurrentUINavigationData()
        {
            if (m_NavigationDataStack != null && m_NavigationDataStack.Count >0)
            {
                return m_NavigationDataStack[m_NavigationDataStack.Count - 1];
            }
            return null;
        }

        public void RegisterEscapeHandle(EscapeHandle handle)
        {
            if(m_EscapeHandles == null)
            {
                m_EscapeHandles = new List<EscapeHandle>();
            }

            m_EscapeHandles.Add(handle);
        }
        public void UnRegisterEscapeHandle(EscapeHandle handle)
        {
            if(m_EscapeHandles!=null
                && m_EscapeHandles.Count>0)
            {
                m_EscapeHandles.Remove(handle);
            }
        }

        public bool DispatchEscapeHandle()
        {
            bool hasHandledEscape = false;
            if (m_EscapeHandles!=null
                &&m_EscapeHandles.Count>0)
            {
                for (int i =0;i<m_EscapeHandles.Count;i++)
                {
                    hasHandledEscape = m_EscapeHandles[i].Invoke();
                    if (hasHandledEscape)
                        break;
                }
            }
            return hasHandledEscape;
        }
    }
}
