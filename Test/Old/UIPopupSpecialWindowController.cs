﻿using System.Collections.Generic;
using UnityEngine;

namespace GCommon
{
    public class UIPopupSpecialWindowController : UIPopupWindowController
    {
        protected override string AnimationConfogPath()
        {
            return "UI/PopupMessageBoxAnimConfig";
        }

        protected override int MinGroupDepth()
        {
            return 50;
        }

        protected override bool isMessageBox()
        {
            return true;
        }

        protected override void OnUIOpen()
        {
            base.OnUIOpen();
        }

        public virtual void OnBackButtonClick() //just used for Android Back button
        {

        }
    }
}
