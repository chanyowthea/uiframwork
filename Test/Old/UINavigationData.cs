﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GCommon
{
    public class UINavigationData 
    {
        private string m_Title = "";

        public string Title
        {
            get { return m_Title; }
            set { m_Title = value; }
        }

        private int m_NavIndex = 0;

        public int NavIndex
        {
            get { return m_NavIndex; }
            set { m_NavIndex = value; }
        }

        private Type m_ContentControllerType;

        public Type ContentControllerType
        {
            get { return m_ContentControllerType; }
            set { m_ContentControllerType = value; }
        }

        private bool m_HideTopBar = false;

        public bool HideTopBar
        {
            get { return m_HideTopBar; }
            set { m_HideTopBar = value; }
        }

        //extra parameter can be passed to next page
        private List<Object> m_WindowParam = new List<Object>();
        public List<Object> WindowParam
        {
            get
            {
                return m_WindowParam;
            }

            set
            {
                m_WindowParam = value;
            }

        }

        private List<UIPopupWindowController> m_SavedPopupList = new List<UIPopupWindowController>(); //save popup list, show when click back button

        public List<UIPopupWindowController> SavedPopupList
        {
            get { return m_SavedPopupList; }
        }

        public void SavePopupList(List<UIPopupWindowController> popupList)
        {
            m_SavedPopupList.Clear();
            for (int i = 0; i < popupList.Count; i++)
            {
                var ctrl = popupList[i];
                if (ctrl != null && ctrl.gameObject.activeSelf && ctrl.CanSavedByNavigation())
                {
                    m_SavedPopupList.Add(popupList[i]);
                    popupList[i].Hide();
                }
            }
        }

        private bool m_ShowAvatarWindow = true;
        private bool m_ShowAvatarPose = false;

        public bool ShowAvatarWindow
        {
            get { return m_ShowAvatarWindow; }
            set { m_ShowAvatarWindow = value; }
        }

        public bool ShowAvatarPose
        {
            get { return m_ShowAvatarPose; }
            set { m_ShowAvatarPose = value; }
        }
        public UINavigationData()
        {

        }

        public UINavigationData(string customizeTitle)
        {
            m_Title = customizeTitle;
        }

        public override string ToString()
        {
            return string.Format("Navigation {0} {1} {2}", m_NavIndex, m_Title,
                m_ContentControllerType == null ? "null" : m_ContentControllerType.FullName);
        }
    }
}
