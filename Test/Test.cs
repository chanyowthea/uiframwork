﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LogUtil;
using System;
using System.Reflection;
using UIFramwork;

public class TestView : BaseView
{

}

public class TestConfig
{
    public void Get<T>()
    {
        Debug.Log("type=" + typeof(T)); 
    }
}

public class Test : MonoBehaviour
{
    private void Start()
    {
        //Run(typeof(TestView)); 
    }
    
    public void Run(Type t)
    {
        TestConfig conf = new TestConfig(); 
        MethodInfo mi = conf.GetType().GetMethod("Get").MakeGenericMethod(t);
        mi.Invoke(conf, null);
    }
}
