﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UIFramwork;

public class HUDView : BaseView
{
    public void OnClickBack()
    {
        GameManager._instance._viewManager.Close(this.GetHashCode());
    }

    public void OnClickEnd()
    {
        GameManager._instance._viewManager.Open<EndView>();
    }

    public void OnClickPrompt()
    {
        GameManager._instance._viewManager.Open<PromptView>();
    }

    public void OnClickMall()
    {
        //GameManager._instance._viewManager.Open<EndView>();
    }
}
