﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UIFramwork;

public class PromptView : BaseView
{
    public void OnClickClose()
    {
        GameManager._instance._viewManager.Close(this.GetHashCode());
    }
}
