﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UIFramwork;

public class StartView : BaseView
{
    public void OnClickStart()
    {
        GameManager._instance._viewManager.Open<HUDView>(); 
    }
}
