﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UIFramwork;

public class EndView : BaseView
{
    public void OnClickEnd()
    {
        GameManager._instance._viewManager.Close(this.GetHashCode());
    }

    public void OnClickLogout()
    {
        var v = GameManager._instance._viewManager.Open<StartView>(true);
    }
}
