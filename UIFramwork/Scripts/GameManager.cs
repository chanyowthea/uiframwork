﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UIFramwork; 

public class GameManager : MonoBehaviour
{
    [SerializeField] public ViewLibrary _viewLibrary;
    public ViewManager _viewManager;

    public static GameManager _instance;

    private void Awake()
    {
        _instance = this; 
    }

    void Start()
    {
        _viewManager = new ViewManager();
        _viewManager._viewLibrary = _viewLibrary;
        _viewManager.Open<StartView>(); 
    }
}
